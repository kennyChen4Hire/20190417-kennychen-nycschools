//
//  SchoolsTableViewController.swift
//  20190417-KennyChen-NYCSchools
//
//  Created by Kenny Chen on 4/17/19.
//  Copyright © 2019 none. All rights reserved.
//



// very quick implementation. If i had time i'd make a custom tableview cell and separate the viewcell and its customizations. Had i had more time i'd also fix the size of the tableview a little better. Also implement "infinity" scroll to be more performant. 


import UIKit

struct cellInfo {
    var collapsed = Bool()
    var title = String()
    var sectionData = [String]()
}

class SchoolsTableViewController: UITableViewController {
    
    
    var tableData = [cellInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // network call to create data model
        
        guard let dataString = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else {return}
        let task = URLSession.shared.schoolsTask(with: dataString) { (schools, response, error) in
            if let schools = schools {
                schools.forEach({ (school) in
                    let newSchool = cellInfo(collapsed: false, title: school.schoolName, sectionData: ["SAT MATH: \(school.math)","SAT WRITING: \(school.writing)","SAT READING: \(school.reading)"])
                    self.tableData.append(newSchool)
                    print(newSchool)
                })
            }
            if let error = error {
                print(error)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // count of test data +1 to include the section row
        if tableData[section].collapsed == true {
            return tableData[section].sectionData.count + 1
        } else {
        return 1
        // just the school name
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let dataIndex = indexPath.row - 1
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {return UITableViewCell()}
            cell.textLabel?.text = tableData[indexPath.section].title
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {return UITableViewCell()}
            cell.textLabel?.text = tableData[indexPath.section].sectionData[dataIndex]
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //when selected expand to view data
        if tableData[indexPath.section].collapsed == true {
            tableData[indexPath.section].collapsed = false
            let sections = IndexSet(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            tableData[indexPath.section].collapsed = true
            let sections = IndexSet(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
            }
        }
    }
}
