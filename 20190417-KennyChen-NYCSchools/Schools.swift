//
//  Schools.swift
//  20190417-KennyChen-NYCSchools
//
//  Created by Kenny Chen on 4/17/19.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation


typealias Schools = [School]

struct School: Codable {
    let dbn, schoolName, numOfSatTestTakers, reading,math, writing: String
    enum CodingKeys: String, CodingKey {
        case dbn
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
        case schoolName = "school_name"
    }
    
}


extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? JSONDecoder().decode(T.self, from: data), response, nil)
        }
    }
    
    func schoolsTask(with url: URL, completionHandler: @escaping (Schools?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
